<!-- Start features84 -->
@if(!Fir\Utils\Helpers::isProduction())
<!-- Description : TBD -->
@endif
<section class="{{ $block->classes }}" is="fir-features-84" id="{{ $pinecone_id ?? '' }}" data-title="{{ $pinecone_title ?? '' }}" data-observe-resizes>
  <div class="features-84 {{ $bg_color }} {{ $theme }} {{ $text_align }} {{ $flip }}">
      <div class="features-84__content">

        <h6 class="features-84__post-category">Featured {{ substr($post_type, 0, -1) }}</h6>

        <h2>{{ $featured_post->post_title }}</h2>

        <p>{!! $custom['project']['project_excerpt'] !!}</p>

        <a class="features-84__cta btn btn--full" href="{{ get_permalink($featured_post) }}">Learn More</a>
      </div>

      <div class="features-84__img-wrapper">
        <img class="features-84__image" src="{{ get_the_post_thumbnail_url($featured_post) }}" alt="{{ $featured_post->post_title }}">
      </div>
  </div>
</section>
<!-- End features84 -->
