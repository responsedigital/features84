<?php

namespace Fir\Pinecones\Features84\Blocks;

use Log1x\AcfComposer\Block;
use StoutLogic\AcfBuilder\FieldsBuilder;
use Fir\Utils\GlobalFields as GlobalFields;
use function Roots\asset;

class Features84 extends Block
{
    /**
     * The block name.
     *
     * @var string
     */
    public $name = 'Featured Post';

    /**
     * The block view.
     *
     * @var string
     */
    public $view = 'Features84.view';

    /**
     * The block description.
     *
     * @var string
     */
    public $description = '';

    /**
     * The block category.
     *
     * @var string
     */
    public $category = 'fir';

    /**
     * The block icon.
     *
     * @var string|array
     */
    public $icon = 'editor-ul';

    /**
     * The block keywords.
     *
     * @var array
     */
    public $keywords = ['features84'];

    /**
     * The block post type allow list.
     *
     * @var array
     */
    public $post_types = [];

    /**
     * The parent block type allow list.
     *
     * @var array
     */
    public $parent = [];

    /**
     * The default block mode.
     *
     * @var string
     */
    public $mode = 'preview';

    /**
     * The default block alignment.
     *
     * @var string
     */
    public $align = '';

    /**
     * The default block text alignment.
     *
     * @var string
     */
    public $align_text = '';

    /**
     * The default block content alignment.
     *
     * @var string
     */
    public $align_content = '';

    /**
     * The supported block features.
     *
     * @var array
     */
    public $supports = [
        'align' => array('wide', 'full', 'other'),
        'align_text' => false,
        'align_content' => false,
        'mode' => false,
        'multiple' => true,
        'jsx' => true,
    ];

    /**
     * The block preview example data.
     *
     * @var array
     */
    public $defaults = [
        'content' => [
            'featured_post' => null,
            'bg_color' => 'white'
        ]
    ];

    /**
     * Data to be passed to the block before rendering.
     *
     * @return array
     */
    public function with()
    {
        $data = $this->parse(get_fields());
        $newData = [
        ];

        return array_merge($data, $newData);
    }

    private function parse($data)
    {
        $data['featured_post'] = $data['content']['featured_post'] ? get_post($data['content']['featured_post']) : $this->defaults['content']['featured_post'];
        $data['custom'] = \Fir\Utils\Helpers::getMeta($data['featured_post']);
        $data['post_type'] = $data['featured_post'] ? get_post_type($data['featured_post']) : 'post';
        $data['bg_color'] = ($data['content']['bg_color']) ?: $this->defaults['content']['bg_color'];
        $data['flip'] = $data['options']['flip_horizontal'] ? 'features-84--flip' : '';
        $data['text_align'] = 'features-84--' . $data['options']['text_align'];
        $data['theme'] = 'fir--' . $data['options']['theme'];
        return $data;
    }

    /**
     * The block field group.
     *
     * @return array
     */
    public function fields()
    {

        $Features84 = new FieldsBuilder('Featured Post');

        $Features84
            ->addGroup('content', [
                'label' => 'Featured Post',
                'layout' => 'block'
            ])
                ->addPostObject('featured_post', [
                    'label' => 'Post Object Field',
                    'instructions' => 'Select the post to be featured in this space.',
                    'post_type' => ['news', 'services', 'projects'],
                    'taxonomy' => [],
                    'allow_null' => 0,
                    'return_format' => 'array',
                    'ui' => 1,
                ])
                ->addSelect('bg_color', [
                    'choices' => [
                        'white' => 'White',
                        'gray' => 'Gray',
                        'blue' => 'Blue',
                        'teal' => 'Teal'
                    ]
                ])
            ->endGroup()
            ->addGroup('options', [
                'label' => 'Options',
                'layout' => 'block'
            ])
            ->addFields(GlobalFields::getFields('flipHorizontal'))
            ->addFields(GlobalFields::getFields('textAlign'))
            ->addFields(GlobalFields::getFields('theme'))
            ->addFields(GlobalFields::getFields('hideComponent'))
            ->endGroup();

        return $Features84->build();
    }

    /**
     * Assets to be enqueued when rendering the block.
     *
     * @return void
     */
    public function enqueue()
    {
        wp_enqueue_style('sage/app.css', asset('styles/fir/Pinecones/Features84/style.css')->uri(), false, null);
    }
}
