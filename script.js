class Features84 extends window.HTMLDivElement {

    constructor (...args) {
        const self = super(...args)
        self.init()
        return self
    }

    init () {
        this.props = this.getInitialProps()
        this.resolveElements()
      }

    getInitialProps () {
        let data = {}
        try {
            data = JSON.parse(this.querySelector('script[type="application/json"]').text())
        } catch (e) {}
        return data
    }

    resolveElements () {

    }

    connectedCallback () {
        this.initFeatures84()
    }

    initFeatures84 () {
        const { options } = this.props
        const config = {

        }

        // console.log("Init: features84")
    }

}

window.customElements.define('fir-features-84', Features84, { extends: 'section' })
